/*
 * Sample program for DokodeModem
 * trasnsmit and receive
 * Copyright (c) 2024 Circuit Desgin,Inc
 * Released under the MIT license
 */
#include <dokodemo.h>
#include <SlrModem.h>

const uint8_t CHANNEL = 0x10;   // 10進で16チャネルです。通信相手と異なると通信できません。
const uint8_t DEVICE_DI = 0x00; // 通信相手のIDです。0は全てに届きます。
const uint8_t DEVICE_EI = 02;   // 自分のIDです
const uint8_t DEVICE_GI = 0x02; // グループIDです。通信相手と異なると通信できません。

DOKODEMO Dm = DOKODEMO();
SlrModem modem;

void setup()
{
  Dm.begin(); // 初期化が必要です。

  // デバッグ用　USB-Uart
  SerialDebug.begin(115200);

  // modem uart
  UartModem.begin(MLR_BAUDRATE);

  // モデムの電源を入れて少し待ちます
  Dm.ModemPowerCtrl(ON);
  delay(150);

  // モデム操作用に初期化します
  modem.Init(UartModem, nullptr);

  // 各無線設定を行います。電源入り切りするようであればtrueにして内蔵Flashに保存するようにしてください。
  modem.SetMode(SlrModemMode::LoRaCmd, false);
  modem.SetChannel(CHANNEL, false);
  modem.SetDestinationID(DEVICE_DI, false);
  modem.SetEquipmentID(DEVICE_EI, false);
  modem.SetGroupID(DEVICE_GI, false);
}

void loop()
{
  uint8_t recvdata[255];

  modem.Work(); // ループ処理。受信データを処理しています。

  if (modem.HasPacket())
  {
    const uint8_t *pData;
    uint8_t len{0};

    // 受信データのポインタとサイズを取得
    modem.GetPacket(&pData, &len);

    // 受信データ取得
    memcpy(&recvdata[0], pData, len);

    // 受信データ開放
    modem.DeletePacket();

    SerialDebug.println("\r\n*** received ***");

    // 受信データ出力。ここを必要な処理に変更してください。
    // 受信データのチェックは必要です。下記のようなのは誤動作の元です。
    SerialDebug.write(recvdata, len);
    SerialDebug.println();

    // 必要に応じて受信した時のRSSI値を読みます。
    int16_t rssi{0};
    modem.GetRssiLastRx(&rssi);
    SerialDebug.println("Rx RSSI:" + String(rssi) + "dBm");

    // 必要に応じて環境RSSI値を読みます
    int16_t env_rssi{0};
    modem.GetRssiCurrentChannel(&env_rssi);
    SerialDebug.println("Env RSSI:" + String(env_rssi) + "dBm");

    // 受信データを確認して応答します。
    if (recvdata[0] == 'R' && recvdata[1] == 'E' && recvdata[2] == 'Q' && len == 8)
    {
      // 他の通信データが届いても誤動作しない仕組みが必要です。下記のような単純なパケットでは誤動作の元です。
      recvdata[0] = 'A';
      recvdata[1] = 'C';
      recvdata[2] = 'K';

      //@DTコマンドを実行し、送信結果を戻します。@DT08と\r\nは自動で付加されます。
      auto rc = modem.TransmitData((uint8_t *)recvdata, len);
      if (rc == SlrModemError::Ok)
      {
        // 送信完了
        SerialDebug.println("*** Send Ack Ok ***");
      }
      else
      {
        // キャリアセンスによって送信できなかったことを示します。
        // 必要に応じて時間をおいてからリトライ送信してください。
        SerialDebug.println("### Send Ng... ###");
      }
    }
  }
}
